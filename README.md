## Sobre e Autor

Esse projeto foi desenvolvido como trabalho para a matéria de Java para Web. Foi utilizado nesse projeto os conteúdos passados em aula sobre Node.js e Express.js e incrementado com conhecimentos adquiridos através da experiência que eu já possuía trabalhando com essas tecnologias.

Como um plus a esse projeto foi adicionado um pacote chamado Sucrase que é um transpilador que permite trabalhar com as versões mais recentes do JavaScript podendo usar as features recentes como import, export ao invés de require entre outros e também é mais rápido que outros transpiladores como o Babel.

Outro pacote interessante adicionado foi o Yup. Com ele é possível tratar os dados e validar os objetos repassados na api de uma forma eficiente e prática.

Como parte de autenticação foi utilizado a autenticação através de JWT.

## INSTALAÇÃO

Para colocar o projeto em funcionamento é necessário rodar o comando:
`yarn add`

Após a instalação dos pacotes concluídas:
`yarn dev`

## ENDPOINTS

### Usuario

##### Modelo de Dado

    {
     nome: string,
     email: string,
     senha: string,
    }

##### POST

##### Requisição

`http://localhost:3333/usuario`

    {
    "nome": "gilberto",
    "email": "juniortopanotti@gmail.com",
    "senha": "123"
    }

##### Exemplo de resposta

    {
      "_id": "60a1a169cb14df2c5c10ecca",
      "nome": "gilberto",
      "email": "juniortopanotti@gmail.com",
      "senha": "123",
      "__v": 0
    }

##### Erros

`400 e 500`

### Auth

##### Modelo de Dado

    {
     nome: string,
     email: string,
     senha: string,
    }

##### POST

##### Requisição

`http://localhost:3333/auth`

    {
    "email": "juniortopanotti@gmail.com",
    "senha": "123"
    }

##### Exemplo de resposta

    {
      "usuario": {
      "nome": "gilberto",
      "email": "juniortopanotti@gmail.com"
      },
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJub21lIjoiZ2lsYmVydG8iLCJpYXQiOjE2MjEyMDcxMDYsImV4cCI6MTYyMTgxMTkwNn0.XEIWzOBjV0azRyIO1AK-YJXQG6FfRNY9xbpR-mbnRh4"
    }

##### Erros

`400, 401 e 500`

### Professor

##### Modelo de Dado

    {
     cpf: string,
     especialidade: string,
     dt_nascimento: date,
     nome: string
    }

##### POST

##### Requisição

`http://localhost:3333/professor`

    {
    	"cpf": "10566413981",
    	"especialidade": "Matemática",
    	"dt_nascimento": "2017-12-10",
    	"nome": "Gilberto"
    }

##### Exemplo de resposta

    {
      "_id": "60a1a29bc3ed035ff866121f",
      "nome": "Gilberto",
      "cpf": "10566413981",
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "especialidade": "Matemática",
      "__v": 0
    }

##### Erros

`400 e 500`

##### PUT

##### Requisição

`http://localhost:3333/professor/:id`

    {
    	"cpf": "10566413981",
    	"especialidade": "Matemática",
    	"dt_nascimento": "2017-12-10",
    	"nome": "Gilberto"
    }

##### Exemplo de resposta

    {
      "_id": "60a1a29bc3ed035ff866121f",
      "nome": "Gilberto",
      "cpf": "10566413981",
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "especialidade": "Matemática",
      "__v": 0
    }

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/professor`

##### Exemplo de resposta

    [{
      "_id": "60a1a29bc3ed035ff866121f",
      "nome": "Gilberto",
      "cpf": "10566413981",
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "especialidade": "Matemática",
      "__v": 0
    }]

##### Filtros

`page: number, size: number, nome: string`

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/professor/:id`

##### Exemplo de resposta

    {
      "_id": "60a1a29bc3ed035ff866121f",
      "nome": "Gilberto",
      "cpf": "10566413981",
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "especialidade": "Matemática",
      "__v": 0
    }

##### Erros

`400 e 500`

##### DELETE

##### Requisição

`http://localhost:3333/professor/:id`

##### Erros

`204 e 500`

### Estudante

##### Modelo de Dado

    {
     matricula: string,
     nome: string,
     dt_nascimento: date,
     turma: string
    }

##### POST

##### Requisição

`http://localhost:3333/estudante`

    {
     "matricula": "10566413981",
     "dt_nascimento": "2017-12-10",
     "nome": "Gilberto",
     "turma": "60a19b916cc772104c139db5"
    }

##### Exemplo de resposta

    {
      "_id": "60a19c44f965d0406c1e97e3",
      "nome": "Gilberto",
      "matricula": 10566413981,
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "turma": "60a19b916cc772104c139db5",
      "__v": 0
    }

##### Erros

`400 e 500`

##### PUT

##### Requisição

`http://localhost:3333/estudante/:id`

    {
     "matricula": "10566413981",
     "dt_nascimento": "2017-12-10",
     "nome": "Gilberto",
     "turma": "60a19b916cc772104c139db5"
    }

##### Exemplo de resposta

    {
      "_id": "60a19c44f965d0406c1e97e3",
      "nome": "Gilberto",
      "matricula": 10566413981,
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "turma": "60a19b916cc772104c139db5",
      "__v": 0
    }

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/estudante`

##### Exemplo de resposta

    [{
      "_id": "60a19c44f965d0406c1e97e3",
      "nome": "Gilberto",
      "matricula": 10566413981,
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "turma": "60a19b916cc772104c139db5",
      "__v": 0
    }]

##### Filtros

`page: number, size: number, turma: number`

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/estudante/:id`

##### Exemplo de resposta

    {
      "_id": "60a19c44f965d0406c1e97e3",
      "nome": "Gilberto",
      "matricula": 10566413981,
      "dt_nascimento": "2017-12-10T00:00:00.000Z",
      "turma": "60a19b916cc772104c139db5",
      "__v": 0
    }

##### Erros

`400 e 500`

##### DELETE

##### Requisição

`http://localhost:3333/estudante/:id`

##### Erros

`204 e 500`

### Turma

##### Modelo de Dado

    {
     descricao: string,
     ano: number,
     fase: number,
     professores: [string]
    }

##### POST

##### Requisição

`http://localhost:3333/turma`

    {
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "professores": ["60a18ad006fe7349183380ad"]
    }

##### Exemplo de resposta

    {
     "professores": [
     "60a18ad006fe7349183380ad"
     ],
     "_id": "60a1abf8e3672020e4ffca37",
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "__v": 0
    }

##### Erros

`400 e 500`

##### PUT

##### Requisição

`http://localhost:3333/turma/:id`

    {
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "professores": ["60a18ad006fe7349183380ad"]
    }

##### Exemplo de resposta

    {
     "professores": [
     "60a18ad006fe7349183380ad"
     ],
     "_id": "60a1abf8e3672020e4ffca37",
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "__v": 0
    }

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/turma`

##### Exemplo de resposta

    [{
     "professores": [
     "60a18ad006fe7349183380ad"
     ],
     "_id": "60a1abf8e3672020e4ffca37",
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "__v": 0
    }]

##### Filtros

`page: number, size: number, fase: number`

##### Erros

`400 e 500`

##### GET

##### Requisição

`http://localhost:3333/turma/:id`

##### Exemplo de resposta

    {
     "professores": [
     "60a18ad006fe7349183380ad"
     ],
     "_id": "60a1abf8e3672020e4ffca37",
     "descricao": "Turma A",
     "ano": 2021,
     "fase": 9,
     "__v": 0
    }

##### Erros

`400 e 500`

##### DELETE

##### Requisição

`http://localhost:3333/turma/:id`

##### Erros

`204 e 500`

## AUTENTICAÇÃO

Para realizar a autenticação é necessário primeiramente realizar o cadastro de um usuário através da API usuário utilizando o tutorial do endpoint de usuário acima.

Com o usuário criado, basta fazer a requisição no endpoint AUTH com base no tutorial acima, pegar o token retornado na resposta da requisição e utiliza-lo como no header Authorization conforme abaixo:

`Authorization: Bearer SEU_TOKEN`

É extremamente necessário que o Authorization header esteja exatamente como acima, contendo a palavra Bearer no inicio.
