import Turma from "../models/Turma";

class TurmaController {
  constructor() {}

  async save(req, res, next) {
    const { descricao, ano, fase, professores } = req.body;

    const turma = new Turma({
      descricao,
      ano,
      fase,
      professores,
    });

    let saved = null;
    try {
      saved = await turma.save();
    } catch (err) {
      res.status(400).json({ error: err.message });
    }

    return res.json(saved);
  }

  async update(req, res, next) {
    const { id } = req.params;
    const { descricao, ano, fase, professores } = req.body;

    await Turma.findByIdAndUpdate(id, {
      descricao,
      ano,
      fase,
      professores,
    });

    return res.json({ descricao, ano, fase, professores });
  }

  async list(req, res, next) {
    const { page = 1, size = 20, fase } = req.query;
    const query = {};

    if (fase) {
      query.fase = fase;
    }

    const turmas = await Turma.find(query)
      .limit(Number(size))
      .skip(Number((page - 1) * size));

    return res.json(turmas);
  }

  async delete(req, res, next) {
    const { id } = req.params;
    await Turma.findByIdAndDelete(id);

    return res.status(204).json();
  }

  async find(req, res, next) {
    const { id } = req.params;
    const turma = await Turma.findById(id);

    return res.json(turma);
  }
}

export default new TurmaController();
