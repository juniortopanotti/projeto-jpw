import Usuario from "../models/Usuario";

class UsuarioController {
  constructor() {}

  async save(req, res, next) {
    const { nome, email, senha } = req.body;

    const usuario = new Usuario({
      nome,
      email,
      senha,
    });

    const saved = await usuario.save();

    return res.json(usuario);
  }
}

export default new UsuarioController();
