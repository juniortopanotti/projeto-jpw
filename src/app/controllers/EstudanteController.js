import Estudante from "../models/Estudante";

class EstudanteController {
  constructor() {}

  async save(req, res, next) {
    const { nome, matricula, dt_nascimento, turma } = req.body;

    const estudante = new Estudante({
      nome,
      matricula,
      dt_nascimento,
      turma,
    });

    const saved = await estudante.save();

    return res.json(saved);
  }

  async update(req, res, next) {
    const { id } = req.params;
    const { nome, matricula, dt_nascimento, turma } = req.body;

    await Estudante.findByIdAndUpdate(id, {
      nome,
      matricula,
      dt_nascimento,
      turma,
    });

    return res.json({ nome, matricula, dt_nascimento, turma });
  }

  async list(req, res, next) {
    const { page = 1, size = 20, turma } = req.query;
    const query = {};

    if (turma) {
      query.turma = turma;
    }

    const estudantes = await Estudante.find(query)
      .limit(Number(size))
      .skip(Number((page - 1) * size));

    return res.json(estudantes);
  }

  async delete(req, res, next) {
    const { id } = req.params;
    await Estudante.findByIdAndDelete(id);

    return res.status(204).json();
  }

  async find(req, res, next) {
    const { id } = req.params;
    const estudante = await Estudante.findById(id);

    return res.json(estudante);
  }
}

export default new EstudanteController();
