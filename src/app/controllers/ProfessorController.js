import Professor from "../models/Professor";

class ProfessorController {
  constructor() {}

  async save(req, res, next) {
    const { nome, cpf, dt_nascimento, especialidade } = req.body;

    const professor = new Professor({
      nome,
      cpf,
      dt_nascimento,
      especialidade,
    });

    const saved = await professor.save();

    return res.json(saved);
  }

  async update(req, res, next) {
    const { id } = req.params;
    const { nome, cpf, dt_nascimento, especialidade } = req.body;

    await Professor.findByIdAndUpdate(id, {
      nome,
      cpf,
      dt_nascimento,
      especialidade,
    });

    return res.json({ nome, cpf, dt_nascimento, especialidade });
  }

  async list(req, res, next) {
    const { page = 1, size = 20, nome } = req.query;
    const query = {};

    if (nome) {
      query.nome = nome;
    }

    const professores = await Professor.find(query)
      .limit(Number(size))
      .skip(Number((page - 1) * size));

    return res.json(professores);
  }

  async delete(req, res, next) {
    const { id } = req.params;
    await Professor.findByIdAndDelete(id);

    return res.status(204).json();
  }

  async find(req, res, next) {
    const { id } = req.params;
    const professor = await Professor.findById(id);

    return res.json(professor);
  }
}

export default new ProfessorController();
