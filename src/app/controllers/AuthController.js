import jwt from "jsonwebtoken";
import * as Yup from "yup";
import Usuario from "../models/Usuario";

import authConfig from "../../config/auth";

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
      senha: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: "Email ou usuário inválidos!" });
    }

    const { email, senha } = req.body;

    const usuario = await Usuario.findOne({ email, senha });

    if (!usuario) {
      return res.status(401).json({ message: `E-mail ou senha inválidos.` });
    }

    return res.json({
      usuario: {
        nome: usuario.nome,
        email: usuario.email,
      },
      token: jwt.sign({ nome: usuario.nome }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }

  async get(req, res) {
    const { id } = req.userContext;
    const usuario = await Usuario.findByPk(id);

    const { nome, tipo, email } = usuario;

    return res.json({
      id,
      email,
      nome,
      tipo,
    });
  }
}

export default new SessionController();
