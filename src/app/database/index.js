import Mongoose from "mongoose";
import "../models/Estudante";
import "../models/Professor";
import "../models/Estudante";
import "../models/Usuario";

const DB_URL =
  "mongodb+srv://admin:GSOF9fBtd93J8JrE@projeto-jpw.nmewd.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

class Database {
  constructor() {
    this.init();
  }

  init() {
    Mongoose.connect(DB_URL, { useNewUrlParser: true });

    this.events();
  }

  events() {
    Mongoose.connection.on("connected", () => {
      console.log("Conexão com o banco de dados estabelecida com sucesso!");
    });

    Mongoose.connection.on("disconnected ", () => {
      console.log("Conexão com o banco de dados encerrada!");
    });

    Mongoose.connection.on("error ", () => {
      console.log(
        "Ocorreu um erro ao tentar se conectar com o banco de dados!"
      );
    });
  }
}

export default new Database();
