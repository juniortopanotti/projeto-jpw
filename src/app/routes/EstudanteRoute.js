import EstudanteController from "../controllers/EstudanteController";
import EstudanteValidator from "../validators/EstudanteValidator";

class EstudanteRoute {
  static create(router) {
    router.post(
      "/estudante",
      EstudanteValidator.save,
      EstudanteController.save
    );
    router.get("/estudante", EstudanteController.list);
    router.get(
      "/estudante/:id",
      EstudanteValidator.find,
      EstudanteController.find
    );
    router.delete(
      "/estudante/:id",
      EstudanteValidator.find,
      EstudanteController.delete
    );
    router.put(
      "/estudante/:id",
      EstudanteValidator.find,
      EstudanteValidator.save,
      EstudanteController.update
    );
  }
}

export default EstudanteRoute;
