import { Router } from "express";

import authMiddleware from "../middlewares/Auth";

import TurmaRoute from "./TurmaRoute";
import EstudanteRoute from "./EstudanteRoute";
import ProfessorRoute from "./ProfessorRoute";
import UsuarioValidator from "../validators/UsuarioValidator";
import UsuarioController from "../controllers/UsuarioController";
import AuthController from "../controllers/AuthController";

const routes = [TurmaRoute, EstudanteRoute, ProfessorRoute];

class Routes {
  constructor() {
    this.init();
  }

  openRoutes() {
    this.router.post("/usuario", UsuarioValidator.save, UsuarioController.save);
    this.router.post("/auth", AuthController.store);
  }

  defaultMiddlewares() {
    this.router.use(authMiddleware);
  }

  init() {
    this.router = new Router();

    this.openRoutes();

    this.defaultMiddlewares();

    routes.map((route) => route.create(this.router));
  }
}

export default new Routes().router;
