import ProfessorController from "../controllers/ProfessorController";
import ProfessorValidator from "../validators/ProfessorValidator";

class ProfessorRoute {
  static create(router) {
    router.post(
      "/professor",
      ProfessorValidator.save,
      ProfessorController.save
    );
    router.get("/professor", ProfessorController.list);
    router.get(
      "/professor/:id",
      ProfessorValidator.find,
      ProfessorController.find
    );
    router.delete(
      "/professor/:id",
      ProfessorValidator.find,
      ProfessorController.delete
    );
    router.put(
      "/professor/:id",
      ProfessorValidator.find,
      ProfessorValidator.save,
      ProfessorController.update
    );
  }
}

export default ProfessorRoute;
