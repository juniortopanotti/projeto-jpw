import TurmaController from "../controllers/TurmaController";
import TurmaValidator from "../validators/TurmaValidator";

class TurmaRoute {
  static create(router) {
    router.post("/turma", TurmaValidator.save, TurmaController.save);
    router.get("/turma", TurmaController.list);
    router.get("/turma/:id", TurmaValidator.find, TurmaController.find);
    router.delete("/turma/:id", TurmaValidator.find, TurmaController.delete);
    router.put(
      "/turma/:id",
      TurmaValidator.find,
      TurmaValidator.save,
      TurmaController.update
    );
  }
}

export default TurmaRoute;
