import Mongoose from "mongoose";

class Professor {
  constructor() {
    const schema = new Mongoose.Schema({
      nome: { type: String, required: true },
      dt_nascimento: { type: Date, required: true },
      especialidade: { type: String, required: true },
      cpf: { type: String, required: true },
    });
    this.professor = Mongoose.model("Professor", schema);
  }
}

export default new Professor().professor;
