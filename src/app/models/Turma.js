import Mongoose from "mongoose";

class Turma {
  constructor() {
    const schema = new Mongoose.Schema({
      descricao: { type: String, required: true },
      ano: { type: Number, required: true },
      fase: { type: Number, required: true },
      professores: [
        {
          type: Mongoose.Schema.Types.ObjectId,
          ref: "Prefessor",
          unique: true,
        },
      ],
    });
    this.turma = Mongoose.model("Turma", schema);
  }
}

export default new Turma().turma;
