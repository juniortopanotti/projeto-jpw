import Mongoose from "mongoose";

class Estudante {
  constructor() {
    const schema = new Mongoose.Schema({
      nome: { type: String, required: true },
      dt_nascimento: { type: Date, required: true },
      matricula: { type: Number, required: true },
      turma: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: "Turma",
      },
    });
    this.estudante = Mongoose.model("Estudante", schema);
  }
}

export default new Estudante().estudante;
