import Mongoose from "mongoose";

class Usuario {
  constructor() {
    const schema = new Mongoose.Schema({
      nome: { type: String, required: true },
      email: { type: String, required: true },
      senha: { type: String, required: true },
    });
    this.usuario = Mongoose.model("Usuario", schema);
  }
}

export default new Usuario().usuario;
