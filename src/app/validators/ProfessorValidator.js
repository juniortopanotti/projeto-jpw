import * as Yup from "yup";
import Professor from "../models/Professor";

class ProfessorValidator {
  async save(req, res, next) {
    const schema = Yup.object().shape({
      nome: Yup.string()
        .max(100, "O nome não pode exceder 100 caracteres")
        .required("É obrigatório informar o nome"),
      dt_nascimento: Yup.date("Formato de data inválido.").required(
        "É obrigatório informar a data."
      ),
      especialidade: Yup.string()
        .max(100, "A especialidade não pode exceder 70 caracteres")
        .required("É obrigatório informar a especialidade"),
      cpf: Yup.string()
        .max(11, "0 cpf não pode exceder 11 caracteres")
        .required("É obrigatório informar o cpf"),
    });

    try {
      await schema.validate(req.body);
    } catch (err) {
      const { path, errors, message } = err;
      return res.status(400).json({ path, errors, message });
    }

    return next();
  }

  async find(req, res, next) {
    const { id } = req.params;

    const professor = await Professor.findById(id);

    if (!professor) {
      return res.status(400).json({
        message: "Não foi encontrador nenhum professor com a id informada",
      });
    }

    return next();
  }
}

export default new ProfessorValidator();
