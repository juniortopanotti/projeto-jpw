import * as Yup from "yup";
import Estudante from "../models/Estudante";
import Turma from "../models/Turma";

class EstudanteValidator {
  async save(req, res, next) {
    const schema = Yup.object().shape({
      nome: Yup.string()
        .max(100, "O nome não pode exceder 100 caracteres")
        .required("É obrigatório informar o nome"),
      dt_nascimento: Yup.date("Formato de data inválido.").required(
        "É obrigatório informar a data."
      ),
      matricula: Yup.number().required("É obrigatório informar a matricula"),
      turma: Yup.string().required("É obrigatório informar a turma"),
    });

    try {
      await schema.validate(req.body);
    } catch (err) {
      const { path, errors, message } = err;
      return res.status(400).json({ path, errors, message });
    }

    const turma = await Turma.findById(req.body.turma);
    if (!turma) {
      return res.status(400).json({
        error: "Não foi encontrado nenhuma turma com o id informado",
      });
    }

    return next();
  }

  async find(req, res, next) {
    const { id } = req.params;

    const estudante = await Estudante.findById(id);

    if (!estudante) {
      return res.status(400).json({
        message: "Não foi encontrado nenhum estudante com o id informada",
      });
    }

    return next();
  }
}

export default new EstudanteValidator();
