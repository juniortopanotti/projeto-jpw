import * as Yup from "yup";
import Turma from "../models/Turma";
import Professor from "../models/Professor";

class ProfessorValidator {
  async save(req, res, next) {
    const schema = Yup.object().shape({
      descricao: Yup.string()
        .max(100, "A descrição não pode exceder 100 caracteres")
        .required("É obrigatório informar a descrição"),
      ano: Yup.number().required("É obrigatório informar o ano."),
      fase: Yup.number().required("É obrigatório informar a fase."),
    });

    try {
      await schema.validate(req.body);
    } catch (err) {
      const { path, errors, message } = err;
      return res.status(400).json({ path, errors, message });
    }

    const { professores } = req.body;

    for (let professor of professores) {
      console.log(professor);
      try {
        await Professor.findById(professor);
      } catch (err) {
        return res
          .status(400)
          .json({ error: "Professor com o id " + professor + " não existe." });
      }
    }

    return next();
  }

  async find(req, res, next) {
    const { id } = req.params;

    const turma = await Turma.findById(id);

    if (!turma) {
      return res.status(400).json({
        message: "Não foi encontrador nenhuma turma com a id informada",
      });
    }

    return next();
  }
}

export default new ProfessorValidator();
