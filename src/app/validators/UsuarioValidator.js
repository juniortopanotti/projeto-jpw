import * as Yup from "yup";
import Usuario from "../models/Usuario";

class UsuarioValidator {
  async save(req, res, next) {
    const schema = Yup.object().shape({
      nome: Yup.string()
        .max(100, "O nome não pode exceder 100 caracteres")
        .required("É obrigatório informar o nome"),
      email: Yup.string().email().required(),
      senha: Yup.string().required(),
    });

    try {
      await schema.validate(req.body);
    } catch (err) {
      const { path, errors, message } = err;
      return res.status(400).json({ path, errors, message });
    }

    return next();
  }
}

export default new UsuarioValidator();
